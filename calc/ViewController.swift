//
//  ViewController.swift
//  calc
//
//  Created by damduc on 2015. 3. 5..
//  Copyright (c) 2015년 damduc. All rights reserved.
//

import Cocoa

class ViewController: NSViewController {

    /* Calculator */
    var calc: Calculator = Calculator()
    
    @IBOutlet weak var displayField: NSTextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        displayField.stringValue = calc.getDisplay()
    }

    override var representedObject: AnyObject? {
        didSet {
        // Update the view, if already loaded.
        }
    }


    @IBAction func clickButtonNumber(sender: AnyObject) {
        let button = sender as NSButton
        let input = Character(button.title)
        
        calc.pushNumbers(input)
        displayField.stringValue = calc.getDisplay()
    }
    
    @IBAction func clickButtonAC(sender: AnyObject) {
        calc.pushAC()
        displayField.stringValue = calc.getDisplay()
    }
    
    @IBAction func clickButtonOperation(sender: AnyObject) {
        let button = sender as NSButton
        let op = Character(button.title)
        
        switch op {
        case "+":
            calc.pushOperations(.add)
        case "-":
            calc.pushOperations(.sub)
        case "*":
            calc.pushOperations(.mul)
        case "/":
            calc.pushOperations(.div)
        default:
            calc.pushOperations(.equal)
        }
        displayField.stringValue = calc.getDisplay()
    }
}

