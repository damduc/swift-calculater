//
//  calculator.swift
//  calcase
//
//  Created by damduc on 2015. 3. 5..
//  Copyright (c) 2015년 damduc. All rights reserved.
//

import Foundation

public enum operatations {
    case add
    case sub
    case mul
    case div
    case equal
}

public class Calculator {
    enum calcState {
        case firstStage
        case secondStage
        case thirdStage
    }
    private var status: calcState = .firstStage
    
    private var number1: String = "0"
    private var number2: String = ""
    private var opcode: operatations? = nil
    
    private func calculation() {
        var result: Int
        
        if let op = opcode {
            switch(op) {
            case .add:
                result = number1.toInt()! + number2.toInt()!
            case .sub:
                result = number1.toInt()! - number2.toInt()!
            case .mul:
                result = number1.toInt()! * number2.toInt()!
            case .div:
                result = number1.toInt()! / number2.toInt()!
            default:
                result = number1.toInt()!
            }
            number1 = String(result)
        }
    }
    
    private func clearStatus(flag: Bool) {
        if flag {
            number1 = "0"
        }
        
        number2 = ""
        opcode = nil
        status = .firstStage
    }
    
    public func getDisplay() -> String {
        switch status {
        case .firstStage, .secondStage:
            return number1
        case .thirdStage:
            return number2
        }
    }
    
    public func pushNumbers(num: Character) {
        switch status {
        case .firstStage:
            if number1 == "0" {
                number1 = ""
            }
            number1.append(num)
        case .secondStage:
            if num == "0" {
                return
            }
            number2.append(num)
            status = .thirdStage
        case .thirdStage:
            number2.append(num)
        }
    }
    
    public func pushOperations(op: operatations) {
        switch status {
        case .firstStage:
            status = .secondStage
            opcode = op
        case .secondStage:
            opcode = op
        case .thirdStage:
            calculation()
            if op == .equal {
                clearStatus(false)
            } else {
                opcode = op
                status = .secondStage
            }
        }
    }
    
    public func pushAC() {
        clearStatus(true)
    }
}






